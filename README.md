# University of Arizona Java 8 and Tomcat 7 Docker Container

---

This repository is for the Kuali team's Tomcat 7 image based on their CentOS 6 + Java 8 Docker image.

### Description
This project defines an image that will be the base image for the Kuali team's KFS and Rice Docker images.

### Requirements
This is based on a **java8** tagged image from the _397167497055.dkr.ecr.us-west-2.amazonaws.com/kuali/tomcat7_ AWS ECR repository.

The included version of Apache Tomcat was downloaded from https://archive.apache.org/dist/tomcat/tomcat-7/v7.0.79/bin/ to reduce external dependencies at build time.

### Building
#### Jenkins
The build command we use is: `docker build -t kuali/tomcat7:java8tomcat7-ua-release-<DATE> --force-rm .`

We then tag and push the image to AWS with the build date. (Example: _397167497055.dkr.ecr.us-west-2.amazonaws.com/kuali/tomcat7:java8tomcat7-ua-release-2018-09-24_).

*Caveat 1:* The Dockerfile will need to be updated with the date of the *java8* image, if a newer one was created.
*Caveat 2:* The Dockerfiles for KFS and Rice will need to be updated to use this new version of the java8tomcat7 image based on the creation date.

Jenkins link: https://kuali-jenkins.ua-uits-kuali-nonprod.arizona.edu/jenkins/job/Development/view/Docker%20Baseline/