# Tomcat7

# Pull base image
FROM 397167497055.dkr.ecr.us-west-2.amazonaws.com/kuali/tomcat7:java8-ua-release-2018-09-24
MAINTAINER U of A Kuali DevOps <katt-support@list.arizona.edu>

# Set environment variables
ENV CATALINA_HOME /opt/tomcat7

# Install updates
RUN yum update -y && \
    rm -rf /var/lib/apt/lists/*

# Manually install Tomcat from included download
COPY bin .
RUN tar -xvf apache-tomcat-7.0.79.tar.gz && \
    rm apache-tomcat-7.0.79.tar.gz && \
    mv apache-tomcat* ${CATALINA_HOME}

RUN chmod +x $CATALINA_HOME/bin/*sh

# Create links that KFS image expects (TODO should be vice-versa; update KFS image to be CentOS'y)
RUN ln -s ${CATALINA_HOME} /var/lib/tomcat7
  
EXPOSE 8080
